<# 
#	The following funtions are utilities which automate the process of requesting a certificate from the CA Server.
#	@required:	W2k8-R2 SP1.
#	@author:	Anchit Kalra
#	@date:		7/30/2013
#	@update:	8/28/2013
#>

#. "$support_default_currentSupportPath\support.ps1"
#. "$support_default_currentSupportPath\support_networkConfiguration.ps1"
#. "$support_default_currentSupportPath\support_remote.ps1"
#. "$support_default_currentSupportPath\support_ica.ps1"
#. "$support_default_currentSupportPath\support_pnagent.ps1"
#. "$support_default_currentSupportPath\support_uia.ps1"
#. "$support_default_currentSupportPath\support_laci.ps1"


<# 
#	Installs RSAT-AD-PowerShell on the machine if not already present. This is required by other helper funtions.
#>
Function support_certificateRequest_installActiveDirectoryModule
{
	write-verbose "in function installActiveDirectoryModule"
	import-module ServerManager
	
	try
	{
		Add-WindowsFeature RSAT-AD-PowerShell
	}
	catch
	{
		write-verbose "Caught an exception installActiveDirectoryModule"
		throw "Exception Type: $($_.Exception.GetType().FullName)"
	}
}


<#
# Gets the certificate autority name and FQDN of the default CA in the domain
# Can be retrived by $returnedValue.CAName and $returnedValue.Computer
#
# @return $CA </br> $CA.CAName and $CA.Computer
#>
Function support_certificateRequest_GetCertificationAuthority () 
{
    try
    {
		write-verbose "in function GetCertificationAuthority"
    	$domain = ([System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain()).Name
    	$domain = "DC=" + $domain -replace '\.', ", DC="
		write-verbose "Searching for CA in the domain"
    	$CA = [ADSI]"LDAP://CN=Enrollment Services, CN=Public Key Services, CN=Services, CN=Configuration, $domain"
		#added code as the CA LCMLAB is in LCMLABroot 
		#need better code here!
		if($domain -eq "DC=LCMLab, DC=net")
		{
			$CA = [ADSI]"LDAP://CN=Enrollment Services, CN=Public Key Services, CN=Services, CN=Configuration ,DC=LCMLabroot, DC=net"
		}
    	
    	$CAs = $CA.psBase.Children | %{
    		$current = "" | Select CAName, Computer
    		$current.CAName = $_ | %{$_.Name}
    		$current.Computer = $_ | %{$_.DNSHostName}
    		$current
    	}
    }
    catch
    {
        #Change to logs
        write-verbose "Caught an exception while requesting certificate: Could not find CA"
		Throw "Exception Type: $($_.Exception.GetType().FullName)"
            
    }
    
    #Change to logs    
	write-verbose "CA found $CA.CAName"
	return $CAs
}

<#
#	The funtion creates an .INF file which can then be used to create a 
#	a request .req file. The file is stored in c:\temp folder.
#>
Function support_certificateRequest_createINFRequestForWebCertificate
{
	write-verbose "in funciton createINFRequestForWebCertificate"
	$currentDir = "C:\Temp\"
	$computerName = hostname
	$fqdn=[System.Net.Dns]::GetHostEntry([System.Net.Dns]::GetHostName()).HostName
	$DCRequestFileName = "$currentDir$computerName-CertRequest.req"
	$DCRequestINFFileName = "$currentDir$computerName-CertRequest.inf"

	# ***** Create Server Certificate Request File *****
	<#
	#Change the INF values to match your environment 
    #Change to logs
	#>
	write-verbose "Create Server Certificate Request File (CertReq.inf) for $computerName  "

	$DCRequestINF =
@"
;----------------- request.inf -----------------

[Version]

Signature="$Windows NT$

[NewRequest]
Subject="O=Citrix,CN=$fqdn,OU=LCM"
KeySpec = 1
KeyLength = 2048
Exportable = TRUE
MachineKeySet = TRUE
SMIME = FALSE
PrivateKeyArchive = FALSE
UserProtected = FALSE
UseExistingKeySet = FALSE
ProviderName = "Microsoft RSA SChannel Cryptographic Provider"
ProviderType = 12
RequestType = PKCS10
KeyUsage = 0xa0

[EnhancedKeyUsageExtension]

OID=1.3.6.1.5.5.7.3.1 ; this is for Server Authentication

[RequestAttributes]
;
;-----------------------------------------------  
"@
	#write-output "Generating Certificate Request file... $DCRequestINF to $DCRequestINFFileName"

	try
	{
		$DCRequestINF | out-file -filepath $DCRequestINFFileName -Force
	}
	catch
	{
        #Change to logs
		write-verbose "Caught an exception:"
		Throw "Exception Type: $($_.Exception.GetType().FullName)"
		
	}

}

<#
# The function creates a request file using the INF file generated from the createINFRequestForWebCertificate.
# This is the only function which needs to be called as this calls all other helper funtion.
#>
Function support_certificateRequest_requestCertificate
{
	write-verbose "In request certificate"
    $currentDir = "C:\Temp\"
	#My host name
	$computerName = hostname
	
	support_certificateRequest_createINFRequestForWebCertificate
	
	#Name of the .inf ; .req ; .cer files 
	$requestINFFileName = "$currentDir$computerName-CertRequest.inf"
	$requestREQFileName = "$currentDir$computerName-certreq.req"
	$receivedCERFileName = "$currentDir$computerName-cert.cer"
    
		
	#Actual request made using INF file
	write-verbose "requesting cert using INF file $requestINFFileName"
	certreq -new $requestINFFileName $requestREQFileName
	
	#Get my machines CA name and FQDN 
	$certificateAuthority = support_certificateRequest_GetCertificationAuthority
	$certificateAuthorityName = ($certificateAuthority).CAName
	$certificateAuthorityHostName = ($certificateAuthority).Computer
	
	#Select the second CA in the list as it is the closer of the two CAs wrt to the domain.
    if([string]::IsNullOrEmpty($certificateAuthorityName) -or [string]::IsNullOrEmpty($certificateAuthorityHostName))
    {
        $certificateAuthorityName = ($certificateAuthority[1]).CAName
        $certificateAuthorityHostName = ($certificateAuthority[1]).Computer
    }
	
	
	#Submitting the certificate and accepting the certificate returned by the CA
	write-verbose "Submitting the certificate and accepting the certificate returned by the CA"	
	certreq -submit -attrib "CertificateTemplate:webserver" -config "$certificateAuthorityHostName\$certificateAuthorityName"  $requestREQFileName $receivedCERFileName
	certreq �accept $receivedCERFileName
	
}

<#
#	Main funtion which requests and installs the Certificate from the CA on the given domain.
#>
Function support_certificateRequest_mainRequestCertificate
{
	
	try
	{
		write-verbose "in mainRequestCertificate"
		support_certificateRequest_installActiveDirectoryModule
		support_certificateRequest_requestCertificate
		Remove-Item "c:\Temp\*"
	}
	catch
	{
		#Change to logs
        #Need to add some exception handling. Probably write logs.
		write-verbose "Caught an exception in mainRequestCertificate"
		Throw "Exception Type: $($_.Exception.GetType().FullName)"
		
	}
		
}
#support_certificateRequest_mainRequestCertificate