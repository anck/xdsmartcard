﻿<#DDC configuratin file.
@start date	: 12:06am 2/9/2013
@author		: Anchit Kalra.
@requirment	: W2k8 R2
This script aims at automating the Smart Card Setup for Cloud 9 and One Lab Initiative.
TODO:
1. Install Gemalto Middleware
2. Check SMC Service
3. Install XenDesktop
4. Configure SSL on farm
5. Bind Website -> https -> Configure SSL certificate
6. IIS Manager ActiveDirectoryClientAuthen
7. Citrix GroupPolicy Trust XML request
	Add-PSSnapin Citrix* 
	Set-BrokerSite –TrustRequestsSentToTheXmlServicePort $True
9. Install Gemalto card drivers on VDA
#>


Add-PSSnapin Citrix*
import-module WebAdministration
Import-Module ServerManager

. ".\support_certificaterequest.ps1"

#finds string1 and replaces with sting2
function support_smartcard_ReplaceText([string]$str1, [string]$str2, [string]$filepath)
{
    #Write-host $filepath
	#Write-host $str1
    #Write-host $str2
    $filepath2 = $filepath + "_backup"
    Copy-Item $filepath -destination $filepath2 -Force
	$content = Get-Content $filepath 
    
    $contentNew = $content | %{$_ -replace "$str1$", $str2} 

    $contentNew | out-file -filepath $filepath
	

}

function support_smartcard_Main
{

$computerNameFQDN = [System.Net.Dns]::GetHostEntry([System.Net.Dns]::GetHostName()).HostName
$certificateName = "$computerNameFQDN-cert"
$DDC_NAME =[System.Net.Dns]::GetHostEntry([System.Net.Dns]::GetHostName()).HostName
$websiteName = "Default Web Site"
$WebApplicationName = "InternalXenDesktopSSL"
$webConfFilePath = "C:\inetpub\wwwroot\Citrix\$WebApplicationName\conf\WebInterface.conf"

#DEBUG: ***Do this only if running the script again
#Remove-WebBinding -Name "Default Web Site" -IP "*" -Port 443 -Protocol https


#Remove all certificates from personal store.
$store = New-Object System.Security.Cryptography.X509Certificates.X509Store “MY”,”LocalMachine”
$store.Open(“ReadWrite”)
foreach ($Curr in $store.Certificates){ $store.Remove($curr)   }

#4. Configure SSL on farm
support_certificateRequest_mainRequestCertificate
    
    #Creating a new site to be bound with SSL certificate.
    try
    {
		  & "$env:SystemDrive\Program Files (x86)\Citrix\Web Interface\5.4.0\sitemgr.exe" -c "WIDest=1:/Citrix/InternalXenDesktopSSL,XMLService=$computerNameFQDN`:8080,WIDefaultSite=Yes"
    }
    catch
    {
        if ($lastExitCode -eq 137)
	    {
            #Change to logs
	        Write-Host "WARNING: The WI and PNA Sites have already been configured on $Env:COMPUTERNAME." -fore Yellow
	    }
	    else
        {
            #Change to logs
            Write-Error "Failed to Configure Web Interface site. Error = $lastExitCode"
            write-Error "Exception Type: $($_.Exception.GetType().FullName)" -ForegroundColor Red
        }
    }
		
        
    try
	{            
			
    	#5. Binding the Webpage with https
    	New-WebBinding -Name "Default Web Site" -IP "*" -Port 443 -Protocol https
    	Get-WebBinding 'Default Web Site'
    	
        #6.IIS Manager ActiveDirectoryClientAuthen
    	Add-WindowsFeature Web-Client-Auth
       	
        $cert_Thumbprint = (get-childitem cert:\localmachine\my | WHERE-Object {$_.Subject -like "*$computerName*"} | Select-Object -First 1).thumbprint
        #Change to logs
        Write-Host "My thumprint is $cert_Thumbprint"
        Set-Location IIS:\SslBindings
        #dir
    	get-item "cert:\LocalMachine\MY\$cert_Thumbprint" | new-item 0.0.0.0!443
        Set-Location \
        Set-Location c:
        
        
        Set-WebConfiguration -Filter 'system.webServer/security/authentication/clientCertificateMappingAuthentication' -Value True
        #Set-WebConfiguration -Location "Default Web Site" -Filter "system.webserver/security/access" -Value "Ssl,SslNegotiateCert"
        Set-WebConfiguration -Location "$websiteName/Citrix/$WebApplicationName" -Filter 'system.webserver/security/access' -Value "Ssl,SslNegotiateCert"
    	
    	
    	<#7. Citrix GroupPolicy Trust XML request#> 
    	Set-BrokerSite –TrustRequestsSentToTheXmlServicePort $True
        #support_smartcard_ReplaceText "WIAuthenticationMethods=Explicit" "WIAuthenticationMethods=Explicit,Certificate" $webConfFilePath
        
   	}
	catch 
	{
        #Change to logs
        Write-Error "Failed to Configure Web Interface site. Error = $lastExitCode"
        write-Error "Exception Type: $($_.Exception.GetType().FullName)" 
        write-Error "Exception Message: $($_.Exception.Message)"
	}

}

support_smartcard_Main
