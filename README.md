I had written this to automate setting up smart cards for Xen Desktop 5.6\7.x. The only other change after running this is the StoreFront SSL for XD7.x. Although a very specific use case, there is fair bit of code which can be used to automate other certificate realted tasks. There are generic function which help automate sending a certificate request and then installing the certificate on a local machine.

There are two files,

1. support_certificaterequest.ps1: This file contains functions which automate the process of requesting a certificate from the CA Server.
2. support_smartcard.ps1 :  Calls the certificaterequest.ps1 to request certificates from the domain’s Certificate Authority and binds the certificate to the web interface.

[Blog](https://anchitkalra.wordpress.com/2015/10/20/99/)